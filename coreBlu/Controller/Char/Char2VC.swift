//
//  Char2VC.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit
import CoreBluetooth

class Char2VC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var valueLabel: UILabel!
    var char: CBCharacteristic?
    var peri: CBPeripheral?
    var newValue: Data?
    
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabels(_:)), name: Notify.notificationName, object: nil)
        updateLabels(_: Notification(name: Notify.notificationName))
        
        // Do any additional setup after loading the view.
    }
    
    @objc func updateLabels(_ notification: Notification){
        textField.placeholder = "1 byte"
        guard let characteristicData = char?.value else {return}
        let byteArray = [UInt8](characteristicData)
        let value = String(byteArray[0])
        valueLabel.text = "old value: " + value + " insert new:"
    }
    
    
    @IBAction func textFieldAction(_ sender: Any) {
    }
    @IBAction func closeButton(_ sender: Any) {
        var numericValue = UInt8(textField.text!)
        newValue =  Data.init(bytes: &numericValue, count: 1)//data(using: .utf8)
        if !newValue!.isEmpty && newValue?.count == 1{
            peri?.writeValue(newValue!, for: char!, type: CBCharacteristicWriteType.withResponse)
        }
        dismiss(animated: true, completion: nil)
    }
}
    
