//
//  Char1VC.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit

class Char1VC: UIViewController {
    var receivedLabel: String?
    @IBOutlet weak var valueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valueLabel.text = "The characteristic has value: " + receivedLabel!
        // Do any additional setup after loading the view.
    }
    

    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
