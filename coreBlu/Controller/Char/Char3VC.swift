//
//  Char3VC.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit
import CoreBluetooth
import Charts

class Char3VC: UIViewController {

    @IBOutlet weak var graphView: UIView!
    
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    var char: CBCharacteristic?
    var peri: CBPeripheral?
    var newValue: Data?
    var counter: Double = 0
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    var lineChartEntry1 = [ChartDataEntry]()
    var lineChartEntry2 = [ChartDataEntry]()
    
    
    @objc func updateLabels(_ notification: Notification){
        guard let characteristicData = char?.value else {return}
        let byteArray = [UInt8](characteristicData)
        if !byteArray.isEmpty{
//            transform byte from uint8 to uint8
            let comb1 = (UInt16(byteArray[0]) * 256) + UInt16(byteArray[1])
            let comb2 = (UInt16(byteArray[2]) * 256) + UInt16(byteArray[3])
            leftLabel.text = String(comb1)//String(byteArray[0]) + String(byteArray[1])
            rightLabel.text = String(comb2)//String(byteArray[2]) + String(byteArray[3])
            counter += 1
            lineChartEntry1.append(ChartDataEntry(x: counter, y: Double(comb1)))
            lineChartEntry2.append(ChartDataEntry(x: counter, y: Double(comb2)))
            let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Value")
            let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Value")
            line1.colors = [NSUIColor.green]
            line1.colors = [NSUIColor.yellow]
            let data = LineChartData()
            data.addDataSet(line1)
            data.addDataSet(line2)
            lineChartView.data = data
            }else{
                leftLabel.text = "Ain't got no value"
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLabels(_: Notification(name: Notify.notificationName))
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabels(_:)), name: Notify.notificationName, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

}
