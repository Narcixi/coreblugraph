//
//  TableViewExtension.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth


extension ViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if services.count == 0{
            return peripherals.count
        }else{
            return characteristics.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DiscoveryTVCell.reuseIdentifier, for: indexPath) as? DiscoveryTVCell else{
            return UITableViewCell()
        }
        if services.count==0{
            cell.configureCell(nome: peripherals[indexPath.row].peripheral.description + " " + peripherals[indexPath.row].rSSi.stringValue)
        }else{
            cell.configureCell(nome: characteristics[indexPath.row].description)
//            targetDevice.readValue(for: characteristics[indexPath.row])
        }
        return cell
    }
    
//    selezione dalla tabella della periferica a cui connetterci
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        centralManager.stopScan()
//                target found, now to connect
        if services.count == 0{
            targetDevice = peripherals[indexPath.row].peripheral
            centralManager.connect(targetDevice, options: nil)
        }else{
            let targetChar = characteristics[indexPath.row]
            targetDevice.readValue(for: targetChar)
            switch targetChar.uuid{
                case CBUUID(string: "AA12"):
                    let story = UIStoryboard(name: "Char1", bundle: nil)
                    if let vc = story.instantiateViewController(withIdentifier: "Char1") as? Char1VC {
                        var stringa = "No Value"
                        if targetChar.value != nil {
                            let byteArray = [UInt8](targetChar.value!)
                            stringa = String(byteArray[0])
                        }
                        vc.modalPresentationStyle = .fullScreen
                        vc.receivedLabel = stringa
                        show(vc, sender: self)
                    }
                case CBUUID(string: "AA13"):
                    let story = UIStoryboard(name: "Char2", bundle: nil)
                    if let vc = story.instantiateViewController(withIdentifier: "Char2") as? Char2VC {
                    vc.char = targetChar
                    vc.peri = targetDevice
                    vc.modalPresentationStyle = .fullScreen
                    show(vc, sender: self)
                    targetDevice.readValue(for: targetChar)
                    tableView.reloadData()
                }
                case CBUUID(string: "AB11"):
                    targetDevice.setNotifyValue(true, for: targetChar)
                    targetDevice.readValue(for: targetChar)
                    let story = UIStoryboard(name: "Char3", bundle: nil)
                    if let vc = story.instantiateViewController(withIdentifier: "Char3") as? Char3VC {
                        vc.char = targetChar
                        vc.peri = targetDevice
                        vc.modalPresentationStyle = .fullScreen
                        show(vc, sender: self)
                    }
                default:
                    print("Characteristic not handled, not part of this Challenge")
            }
            
//            targetDevice.readValue(for: targetChar)
        }
        tableView.reloadData()
    }
    
}
