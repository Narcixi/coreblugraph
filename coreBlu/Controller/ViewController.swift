//
//  ViewController.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit
import CoreBluetooth


class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate{
    
    
    
    @IBOutlet weak var scanButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    var centralManager: CBCentralManager!
    var bLEStatus: String = ""
    var targetDevice: CBPeripheral!
    var peripherals: [PeripheralRSSI] = []
    var services: [CBService] = []
    var characteristics: [CBCharacteristic] = []

    
    @IBOutlet weak var scanLabel: UILabel!
    
    @IBAction func scanTapped(_ sender: Any) {
        if !centralManager.isScanning && centralManager.state != .unsupported{
            scanLabel.text = "Scanning for devices, bluetooth status:" + bLEStatus
            centralManager.scanForPeripherals(withServices: nil)//[targetCBUUID])
            scanButton.setTitle("STOP", for: .normal)
            self.peripherals = []
            self.services = []
            self.characteristics = []
        }else{
            centralManager.stopScan()
            scanLabel.text = "Press to scan again"
            scanButton.setTitle("SCAN", for: .normal)
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
          case .unknown:
            bLEStatus = "unknown"
            scanLabel.text = "Scanning for devices, bluetooth status: " + bLEStatus
          case .resetting:
            bLEStatus = "resetting"
            scanLabel.text = "Scanning for devices, bluetooth status: " + bLEStatus
          case .unsupported:
            bLEStatus = "unsupported"
            print("central.state is .unsupported")
          case .unauthorized:
            bLEStatus = "unauthorized"
          case .poweredOff:
            bLEStatus = "poweredOff"
            scanLabel.text = "Bluetooth status: " + bLEStatus
          case .poweredOn:
            bLEStatus = "poweredOn"
        @unknown default:
            print("Defaul Unknown BLE error")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print(peripheral)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
//        fa prima un controllo per verificare che non ci siano peripherals doppione e poi le aggiunge alla lista
        if !peripherals.contains(where: {$0.peripheral == peripheral}){
            let completePeripheral = PeripheralRSSI(peri: peripheral, RSSI: RSSI)
            self.peripherals.append(completePeripheral)
        }
        tableView.reloadData()
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        self.scanLabel.text = "Peripheral connected: " + peripheral.name!
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        tableView.reloadData()
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        self.scanLabel.text = "Failed to connect to: " + peripheral.name!
     }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        self.services = []
        guard let services = peripheral.services else { return }
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
            self.services.append(service)
        }
        tableView.reloadData()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let charats = service.characteristics else {return}
        for char in charats{
            print(char)
            targetDevice.readValue(for: char)
            characteristics.append(char)
        }
        tableView.reloadData()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
//        tableView.reloadData()
        if let error = error {
            print(error)
        }else{
            guard let data = characteristic.value else{return}
            if characteristic.uuid != CBUUID(string: "AA12"){
                print("notifing changes in char")
                NotificationCenter.default.post(name: Notification.Name("UPDATE"), object: self)
                tableView.reloadData()
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            print(error)
        }else{
            tableView.reloadData()
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        tableView.reloadData()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        tableView.reloadData()
        if let error = error {
            print(error)
        }else{
            print("notifing changes")
            characteristic.value
        }
    }
    
    
    @objc func reload(_ notification: Notification){
        tableView.reloadData()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        scanLabel.text = "Press Scan to start scanning"
        centralManager = CBCentralManager(delegate: self, queue: nil)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DiscoveryTVCell", bundle: nil), forCellReuseIdentifier: DiscoveryTVCell.reuseIdentifier)
        NotificationCenter.default.addObserver(self,selector: #selector(reload(_:)), name: Notify.notificationName, object: self)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


