//
//  Peripheral.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import Foundation
import CoreBluetooth
// ho creato questa struct con rssi perchè l'attributo rssi della classe CBperi.. è deprecato, quindi lo salvo qui da readRSSI

struct PeripheralRSSI {
    var  peripheral:CBPeripheral
    var  rSSi:NSNumber
    
    init(peri: CBPeripheral, RSSI: NSNumber){
        self.peripheral = peri
        self.rSSi = RSSI
    }
}

struct Notify{
    static let notificationName = Notification.Name("UPDATE")
}
