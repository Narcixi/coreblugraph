//
//  DiscoveryTVCellTableViewCell.swift
//  coreBlu
//
//  Created by Narci on 16/10/2019.
//  Copyright © 2019 Narci. All rights reserved.
//

import UIKit

class DiscoveryTVCell: UITableViewCell {
    
    static let reuseIdentifier = "DiscoveryTVCell"

    @IBOutlet weak var cellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    func configureCell(nome: String){
        cellLabel.text! = nome
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
