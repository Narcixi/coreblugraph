Hello,

This is a sample project for a challenge, it uses bluetooth to connect to a peripheral with these properties:
    
    Service UUID: 0000AA10-0000-1000-8000-00805F9B34FB:
    	-> Characteristic 1 UUID: 0000AA12-0000-1000-8000-00805F9B34FB (Proprietà: Read) (1 byte)
    	-> Characteristic 2 UUID: 0000AA13-0000-1000-8000-00805F9B34FB (Proprietà: Read, Write) (1 byte)
    Service UUID: 0000AB10-0000-1000-8000-00805F9B34FB:
    	-> Characteristic 1 UUID: 0000AB11-0000-1000-8000-00805F9B34FB (Proprietà: Read, Notify) (4 byte)
	

	It scans for nearby devices, connects to one selected from a tableview, explores it's services/characteristics
	so you can:
	    - read from 0000AA12, and all the others
	    - write into 0000AA13
	    - get notified by 0000AB11
	
	subscribed to AB11 you get updated value everytime it's changed, dinamically changed with an observer, 
	updates 2 label in the lower side of the view and updates a chart with 2 lines from the values.
	
May need cocoapod installed, since it uses https://github.com/danielgindi/Charts for the charts.
	
Works with light and dark theme.	
	
Any feedback will be appreciated, Thanks!